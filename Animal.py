# -*- coding : utf-8 -*-
# @Author : …
# @Date : 2021/7/4 16:47
# @Project  File : pythonProject2  Animal.py

class Animal:
    def __init__(self,name,color,age,sex):
        self.name = name
        self.color = color
        self.age = age
        self.sex = sex
    def call(self):
        print(f"名称{self.name}，颜色{self.color}，年龄{self.age}，性别{self.sex}，会叫")

    def run(self):
        print(f"名称{self.name}，颜色{self.color}，年龄{self.age}，性别{self.sex}，会跑")


# 创建子类猫
class Mao(Animal):
    def __init__(self, name, color, age, sex,hair):
        super().__init__(name, color, age, sex)
        self.hair = hair

    def catch_mouse(self):
        print(f"名称{self.name}，毛发{self.color}{self.hair}，年龄{self.age}岁，性别{self.sex}的猫会捉老鼠")

    def call(self):
        print(f"名称{self.name}，毛发{self.color}{self.hair}，年龄{self.age}岁，性别{self.sex}的猫会喵喵叫")

# 创建子类狗
class Gou(Animal):
    def __init__(self, name, color, age, sex,hair):
        super().__init__(name, color, age, sex)
        self.hair = hair

    def kanjia(self):
        print(f"名称{self.name}，毛发{self.color}{self.hair}，年龄{self.age}岁，性别{self.sex}的狗会看家")

    def call(self):
        print(f"名称{self.name}，颜色{self.color}，年龄{self.age}，性别{self.sex}的狗会汪汪叫")



if __name__ == '__main__':
    cat = Mao("二两","灰色",2,"女","短毛")
    cat.catch_mouse()
    dog = Gou("半斤","灰色",2,"男","长毛")
    dog.kanjia()